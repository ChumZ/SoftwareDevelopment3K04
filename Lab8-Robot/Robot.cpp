#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream> 
#include <queue> 
#include <math.h>

   using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    uint32_t        gPort(63566); 

// Constants
#define PI_TWO  3.14159265358979323846  *2.
#define PI  3.14159265358979323846 
#define PI_HALF  3.14159265358979323846 *.5


///////////////////////////////////////////////////////////////////////////////////////////////////// Super Classes 
class Behaviors{
	public:
                Behaviors(Position2dProxy* pp); // Constructor (Default constructor is private)
                virtual void Init(void){}; // Overwritten by childeren to given them information
                virtual bool Tick(void) =0; //  Called every frame, true is Behavior complete
                //virtual void Resume(void){}; // Resume after other behavior was done
                double GetIntendedSpeed(){return(speed);};
                double GetIntendedRotation(){return(turnrate);};
                void DoMove(void); //Perform the intended move (not overwriten by childen !!)

        protected:
                double speed;	// The intended speed in this frame
		double turnrate; 
                Position2dProxy* posProxy; // The position proxy to obain information !!

        private:
                Behaviors(){}; // Private !!

};

Behaviors::Behaviors(Position2dProxy* pp) {
	posProxy = pp; 
	speed = 0;
	turnrate = 0; 
}

void Behaviors::DoMove(void){ 
	posProxy->SetSpeed(speed, turnrate); 
}


class Collision{
	public: 
		Collision(Position2dProxy* pp, RangerProxy* rp); 
		virtual bool CheckMovement(double speed, double turnrate) =0; // Check to see if bot will crash 
		virtual double DirectionOfFreeSpace(void) =0; // Gives direction with space to move towards
		void GetInfo(void); 
	protected:
		Position2dProxy* posproxy; 
		RangerProxy* ranger; 
}; 

Collision::Collision(Position2dProxy* pp, RangerProxy* rp){
	posproxy = pp;
	ranger = rp; 
} 

void Collision::GetInfo(void) {
	 std::cout << "-------------------------"<<std::endl;
        std::cout << "Ranger has " << ranger->GetElementCount() << "Elements"<<std::endl;
        std::cout << "Ranger has " << ranger->GetRangeCount() << "Rays "<<std::endl;
        std::cout << "Min Angle " << rtod(ranger->GetMinAngle()) <<  std::endl;
        std::cout << "Max Angle " << rtod(ranger->GetMaxAngle()) <<  std::endl;
        std::cout << "Angular Resulution " << rtod(ranger->GetAngularRes()) <<  std::endl;
        std::cout << "Scanning Frequency " << ranger->GetFrequency()  <<  std::endl;
        //double deg_per_angle= (ranger->GetMaxAngle() - ranger->GetMinAngle()) / (double)ranger->GetRangeCount();

        std::cout << "-------------------------"<<std::endl;
} 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////// Sub Classes 
//-============================================================== MoveStraight
#define STRAIGH_MAX_SPEED .5
#define STRAIGH_CRAWL_SPEED .1
#define STRAIGHT_TOLERANCE 0.2

class MoveStraight : public Behaviors {

	public:
		MoveStraight(Position2dProxy* pp);
		~MoveStraight(); 
		void Init(double d) ;
		bool Tick(void) ;
	private: 
		double xf;
		double yf;
		double dist; 
		 

};
MoveStraight::MoveStraight(Position2dProxy* pp):Behaviors(pp) {
	xf=0.;
	yf=0.;
}

MoveStraight::~MoveStraight() {
}

void MoveStraight::Init(double d){
        xf=posProxy->GetXPos()+ d * cos(posProxy->GetYaw());
        yf=posProxy->GetYPos()+ d * sin(posProxy->GetYaw());
}

bool MoveStraight::Tick(void) {
	dist = sqrt((xf - posProxy->GetXPos())*(xf - posProxy->GetXPos()) + (yf - posProxy->GetYPos())*(yf - posProxy->GetYPos()));
 	
	if(dist>1){ // Proportional speed control
 		speed=STRAIGH_MAX_SPEED;
		return(false);
	}

 	if(dist<1 && dist>STRAIGHT_TOLERANCE){
 		speed= dist* STRAIGH_MAX_SPEED +STRAIGH_CRAWL_SPEED;
		return(false); 
	} 
 	
 
	if(dist<STRAIGHT_TOLERANCE){
		speed = 0; 
		turnrate = 0; 
		posProxy->SetSpeed(0,0); 
 		return(true);
	} 
}

// ================================================================== Turn
#define TURN_MAX_SPEED 1. 
#define TURN_CRAWL_SPEED .01
#define TURN_TOLERANCE  .1

class Turn : public Behaviors {
	public:
                Turn(Position2dProxy* pp);
                ~Turn();
                void Init(double a) ;
                bool Tick(void) ;
        private:
		double  turn_to;
};

Turn::Turn(Position2dProxy* pp):Behaviors(pp){
	turn_to=0.;
}

Turn::~Turn(){
}

void Turn::Init(double a){
        turn_to=posProxy->GetYaw() + a;
	if(turn_to>PI) {turn_to=turn_to-PI_TWO;}; 
}

bool Turn::Tick(void){
	double error=fabsf(turn_to-posProxy->GetYaw());
 
	if(error>1.) // Proportional turn speed control
 		turnrate=TURN_MAX_SPEED;
 	else
		turnrate=TURN_MAX_SPEED*error + TURN_CRAWL_SPEED;
 
	if(error<TURN_TOLERANCE){
		posProxy->SetSpeed(0,0); 
 		return(true);
 	}else{
 		return(false);
	}; 
}

// ================================================================== Random
class Random : public Behaviors {
	public: 
		Random(Position2dProxy* pp);
		~Random(); 
		void Init(void) ; 
		bool Tick(void) ; 
	private: 
		double a; 
		double sequence;
}; 

Random::Random(Position2dProxy* pp):Behaviors(pp){
	a = 0.1; 
}

Random::~Random(){
}

void Random::Init(void){ 
	a = 0.1;
	turnrate = 1; 
} 

bool Random::Tick(void){ 
	sequence = rand() %100 - 50;
        turnrate = (1-a)*turnrate+a*sequence; 	
	speed = 0.5; 

	return(false); 
} 

// ================================================================ Move Postion
class MovePosition : public Behaviors {
	public:
		MovePosition(Position2dProxy* pp); 
		~MovePosition ();
		void Init(double x, double y);
		bool Tick(void); 
	private:
		double xf,yf; 
		double distance,angle; 	
		double turnspeed; 
}; 

MovePosition::MovePosition(Position2dProxy* pp):Behaviors(pp) {
	xf = 0;
	yf = 0; 
} 

MovePosition::~MovePosition(){
}

void MovePosition::Init(double x, double y) { 
	xf = x;
	yf = y;
} 

bool MovePosition::Tick(void){ 
	distance = sqrt((xf - posProxy->GetXPos())*(xf - posProxy->GetXPos()) + (yf - posProxy->GetYPos())*(yf - posProxy->GetYPos()));
	angle = atan2(yf - posProxy->GetYPos(),xf - posProxy->GetXPos());
	turnspeed = posProxy->GetYaw() - angle; 

	
	if (distance < 0.2){ 
		speed = 0 ;
		turnrate = 0; 
		posProxy->SetSpeed(speed,turnrate); 
		return(true); 
	}else{ 
		speed = 0.5; 
		if (turnspeed >= PI){
			turnrate = -(turnspeed-2*PI); 
			return(false);
		} else if (turnspeed < -PI){
			turnrate = -(turnspeed+2*PI);
			return(false);
		} else { 
			turnrate = -turnspeed;
			return(false);
		} 
	}
}

// =============================================================== MoveNetwork
class MoveNetwork : public Behaviors {
	public:
		MoveNetwork(Position2dProxy* pp); 
		~MoveNetwork();
		void Init(char* file); 
		bool Tick(void); 
	private: 
		std::queue<double> Coords; 
		double distance, angle; 
		double X,Y;
		double a; 
};

MoveNetwork::MoveNetwork(Position2dProxy* pp):Behaviors(pp){ 
	X = 0;
	Y = 0; 
}

MoveNetwork::~MoveNetwork(){
} 

void MoveNetwork::Init(char* file){
	std::fstream myfile(file, std::ios_base::in);
	while(myfile >> a){
		Coords.push(a);
	}

	X = Coords.front();  
	Coords.pop();

	Y = Coords.front();
	Coords.pop();
} 

bool MoveNetwork::Tick(void){ 
	angle = atan2(Y-posProxy->GetYPos(),X-posProxy->GetXPos());
	distance = sqrt(((posProxy->GetXPos()- X)*(posProxy->GetXPos()- X)+(posProxy->GetYPos()- Y)*(posProxy->GetYPos()- Y)));
	double turnspeed = posProxy->GetYaw() - angle;


	if (Coords.empty() && distance <0.2) { posProxy->SetSpeed(0,0); return(true);}

	if (distance < 0.2){ 
		speed = 0 ;
		turnrate = 0; 
		posProxy->SetSpeed(speed,turnrate); 
		
		X = Coords.front();
		Coords.pop();

		Y = Coords.front();
		Coords.pop();
		return(false);  
	}else{ 
		speed = 0.5; 
		if (turnspeed >= PI){
			turnrate = -(turnspeed-2*PI); 
			return(false);
		} else if (turnspeed < -PI){
			turnrate = -(turnspeed+2*PI);
			return(false);
		} else { 
			turnrate = -turnspeed;
			return(false);
		} 
	}

} 
//= ============================================================ WAIT
class Wait : public Behaviors{
	public:
		Wait(Position2dProxy* pp);
		~Wait();
		bool Tick(void) {return(false);};
}; 

Wait::Wait(Position2dProxy* pp):Behaviors(pp) {
	posProxy = pp;
	speed = 0;
	turnrate = 0; 
}

Wait::~Wait(){
}
// =============================================== Laser
class Laser: public Collision {
	public: 
		Laser(Position2dProxy* , RangerProxy*); 
		bool CheckMovement(double speed, double turnrate);
		double DirectionOfFreeSpace(void);
		 
}; 



//////////////////////////////////////////////////////////////////////////////////////////Main 
int main(int argc, char **argv) {

  try {

    PlayerClient robot(gHostname, gPort); // Connect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)
    RangerProxy rp(&robot, 0); // 0 for sonar, 1 for laser 

    robot.Read(); 
    rp.RequestGeom();
    rp.RequestConfigure();

    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors

    srand(time(NULL));  //Initialize Random Seed
    char p[] = "data.txt";  //Initialize File
    int b = 0; //Counter for behaviours 

    Behaviors* behave; 
    behave = new MoveStraight(&pp); 
    ((MoveStraight*) behave)->Init(5); 

    // go into  a loop
    for(;;){ 

      // this blocks until new data comes; 10Hz by default
      robot.Read();
	
      
      if(behave->Tick()){
	// Change Behaviors When Done
	b=b+1; 

	if (b == 1){ 
	behave = new Turn(&pp);
	((Turn*) behave)->Init(PI/2); 
	}

      }else{
; 	behave->DoMove();
      }
    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}
