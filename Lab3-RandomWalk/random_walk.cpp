// This program is written to implement the random walking motion of a robot. This program is also intended to write the pstion of the bot to a text file and produce a plot of its path. 

#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream> 

    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    // NOTE !!! PLAYER_PORTNUM is the port number and has to be unique for each student,
   // and be set in the plaer server with -p
    uint32_t        gPort(63566); // Replace this with your port number !!


void random_walk(Position2dProxy* pp){    //Produce random motion of bot 
       double turnrate;
       double sequence;
       double a = 0.1;  

       sequence = rand() %100 - 50;
       turnrate = (1-a)*turnrate+a*sequence; 
       
       pp->SetSpeed(5, turnrate); 
	}

void logger(Position2dProxy* pp){      //Creates a log file keeping track of position 
	double xPos = pp->GetXPos(); 
        double yPos = pp->GetYPos();

      	FILE * log = fopen("log.txt", "a"); 
       	fprintf(log, "X: %f        Y:%f\n", xPos, yPos); 
        fclose(log);

	/*std::ofstream out("log.txt");
        out << "X: %f   Y: %f \n",xPos,yPos;
        out.close(); */
       }
	
int main(int argc, char **argv)
{

  // we throw exceptions on creation if we fail
  try {
    PlayerClient robot(gHostname, gPort); // Connect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)

    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors

    //Initialze srand 
    srand(time(NULL));

    //Initialize Movement
    pp.SetSpeed(5,0); 

    // go into  a loop
    for(;;){
      //Procedure Call
      random_walk(&pp); 
      logger(&pp); 

      // this blocks until new data comes; 10Hz by default
      robot.Read();

    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}
