#include <libplayerc++/playerc++.h>
#include <iostream>

    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    uint32_t        gPort(63566);

//Constant Decleration 
double PI = 3.1415926535897; 

void MoveToPosition (Position2dProxy* pp, double XF, double YF, double A){ 
	double xPos = pp->GetXPos();
        double yPos = pp->GetYPos();
        double yaw = pp->GetYaw(); 

	double distance = sqrt((XF - xPos)*(XF - xPos) + (YF - yPos)*(YF - yPos));
	double angle = atan2(YF - yPos,XF - xPos); 
	double turnspeed = yaw - angle; 

	if (distance > 0.2){
		if (turnspeed >= PI){
			pp->SetSpeed(0.2, -(turnspeed-2*PI));
		} else if (turnspeed < -PI){
			pp->SetSpeed(0.2, -(turnspeed+2*PI));
		} else { 
			pp->SetSpeed(0.2, -turnspeed);
		} 
	} else { 
		 if (yaw < A+0.15 && yaw > A-0.15){
                        pp->SetSpeed(0.0, 0);
                }
                else {
                        pp->SetSpeed(0.0, -PI/10);
                }
	}
} 

int main(int argc, char **argv)
{

  try {

    PlayerClient robot(gHostname, gPort); // Connect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)

    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors

    // go into  a loop
    for(;;){
      MoveToPosition(&pp,1,1,1); 

      // this blocks until new data comes; 10Hz by default
      robot.Read();
    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}
