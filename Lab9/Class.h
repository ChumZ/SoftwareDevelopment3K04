#ifndef CLASS_H
#define CLASS_H

#include <libplayerc++/playerc++.h>
#include <queue>

namespace PlayerCc{ 

	class Behaviours{
		public:
                	Behaviours(Position2dProxy* pp); // Constructor (Default constructor is private)
              	 	virtual void Init(void){}; // Overwritten by childeren to given them information
                	virtual bool Tick(void) =0; //  Called every frame, true is Behavior complete
                	virtual void Resume(void){}; // Resume after other behavior was done
                	double GetIntendedSpeed(){return(speed);};
                	double GetIntendedRotation(){return(turnrate);};
                	void DoMove(void); //Perform the intended move 
        	protected:
                	double speed;	// The intended speed in this frame
			double turnrate; 
                	Position2dProxy* posProxy; // The position proxy to obain information !!
		private:
                	Behaviours(){}; // Private !!
};
// =======================
class MoveStraight : public Behaviours {

	public:
		MoveStraight(Position2dProxy* pp);
		~MoveStraight(); 
		void Init(double d) ;
		bool Tick(void) ;
		void Resume(void); 
	private: 
		double xf;
		double yf;
		double distance;	 
};

class Turn : public Behaviours {
	public:
                Turn(Position2dProxy* pp);
                ~Turn();
                void Init(double a) ;
                bool Tick(void) ;
		void Resume(void); 
        private:
		double  turn_to;
		double angle; 
};

class NegTurn : public Behaviours { 
	public:
                NegTurn(Position2dProxy* pp);
                ~NegTurn();
                void Init(double a) ;
                bool Tick(void) ;
		void Resume(void); 
        private:
		double turn_to;
		double angle; 
};

class Random : public Behaviours {
	public: 
		Random(Position2dProxy* pp);
		~Random(); 
		void Init(void) ; 
		bool Tick(void) ; 
	private: 
		double a; 
		double sequence;
}; 

class MovePosition : public Behaviours {
	public:
		MovePosition(Position2dProxy* pp); 
		~MovePosition ();
		void Init(double x, double y);
		bool Tick(void); 
	private:
		double xf,yf; 
		double distance,angle; 	
		double turnspeed; 
}; 

class MoveNetwork : public Behaviours {
	public:
		MoveNetwork(Position2dProxy* pp); 
		~MoveNetwork();
		void Init(char* file); 
		bool Tick(void); 
	private: 
		std::queue<double> Coords; 
		double distance, angle; 
		double X,Y;
		double a; 
};

class Wait : public Behaviours{
	public:
		Wait(Position2dProxy* pp);
		~Wait();
		bool Tick(void) {return(true);};
}; 
// =======================

	class Collision{
		public: 
			Collision(Position2dProxy* pp, RangerProxy* rp); 
			virtual bool CheckMovement(double speed, double turnrate) =0; // Check to see if bot will crash 
			virtual double DirectionOfFreeSpace(void) =0; // Gives direction with space to move towards
		protected:
			Position2dProxy* posProxy; 
			RangerProxy* ranger; 
}; 
// ======================
class Sonar : public Collision{
	public: 
		Sonar(Position2dProxy* pp, RangerProxy* rp); 
		bool CheckMovement(double speed, double turnrate);
		double DirectionOfFreeSpace(void); 
}; 

class Laser : public Collision{
	public:
		Laser(Position2dProxy* pp, RangerProxy* rp); 
		bool CheckMovement(double speed, double turnrate);
		double DirectionOfFreeSpace(void); 
}; 
// ======================
	class Robot{
       		 public:
                	Robot(PlayerClient* client, int index); // Open the robot with a specific index setup everything
			double GetPosX(void); // Return current position of this robot
			double GetPosY(void); 
 			void Tick(void); // Give Computational time
			void Init(void);  //! Initialize the classes, Call before first tick !!!
			virtual Behaviours* ChangeBehaviour(void); //! Overwrite this to create bevavior
                	virtual Behaviours* ChangeCollisionBehaviour(void);//! Overwrite this to create bevavior       
		 protected:
			Robot();
                	Position2dProxy* posProxy; 
                	RangerProxy* ranger;
			Behaviours * current;  // Behaviour to be performed
			Behaviours* collide;  //Behaviour when collision
			Behaviours* standby; //When in collsion
			Collision* colCheck; 
}; 
//=======================
class RobotPath : public Robot {
	public:
		RobotPath(PlayerClient* client, int index); 
		Behaviours* ChangeBehaviour(void);
		Behaviours* ChangeCollisionBehaviour(void); 
	private: 
		enum move{S,L,R};
		move mode; 
		Behaviours* go; 
		Behaviours* manu;
		Behaviours* negmanu;
}; 
//======================= 
class RobotMove : public Robot{
	public:
		RobotMove(PlayerClient* client, int index, double a, double b); 
		Behaviours* ChangeBehaviour(void); 
		Behaviours* ChangeCollisionBehaviour(void); 
	private: 
		enum move{S,L,R};
		move mode; 
		Behaviours* go; 
		Behaviours* manu;
		Behaviours* negmanu;
};
//=======================
}
#endif
	
