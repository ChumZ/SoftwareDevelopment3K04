#include <libplayerc++/playerc++.h>
#include <iostream>
#include <math.h>
#include "Class.h"
 
using namespace PlayerCc;

std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
uint32_t        gPort(63566); 

int main(int argc, char **argv) {

  try {

	PlayerClient robot(gHostname, gPort); // Open Connection to Server 

	//double x = 0;
	//double y = 3; 

	RobotPath* mybot;   //Movment of RObot According to path.txt
	mybot = new RobotPath(&robot, 0); 

	//RobotMove* mybot;    // Movment of Robot to specific point
	//mybot = new RobotMove(&robot, 0, x, y); 

	mybot->Init();

	robot.Read(); // Data Flow 

	for(;;){
		robot.Read(); 
		mybot->Tick();
	}  

  }catch (PlayerCc::PlayerError & e) {
   	 std::cerr << e << std::endl;
    	 return -1;
  }
}

