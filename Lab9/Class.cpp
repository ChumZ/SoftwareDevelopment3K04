#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream>
#include "Class.h"

using namespace PlayerCc; 

// Constants
#define PI_TWO  3.14159265358979323846  *2.
#define PI  3.14159265358979323846 
#define PI_HALF  3.14159265358979323846 *.5

// ===== Behaviour
Behaviours::Behaviours(Position2dProxy* pp) {
	posProxy = pp; 
	speed = 0;
	turnrate = 0; 
}
void Behaviours::DoMove(void){ 
	posProxy->SetSpeed(speed, turnrate); 
}
// ====== MoveStraight 
#define STRAIGH_MAX_SPEED .5
#define STRAIGH_CRAWL_SPEED .1
#define STRAIGHT_TOLERANCE 0.2

MoveStraight::MoveStraight(Position2dProxy* pp):Behaviours(pp) {
	xf=0.;
	yf=0.;
	distance = 0; 
}

MoveStraight::~MoveStraight() {
}

void MoveStraight::Init(double d){
        xf=posProxy->GetXPos()+ d * cos(posProxy->GetYaw());
        yf=posProxy->GetYPos()+ d * sin(posProxy->GetYaw());
	distance = d; 
}

void MoveStraight::Resume(void){
	Init(distance);
}
	
bool MoveStraight::Tick(void) {
	double dist = sqrt((xf - posProxy->GetXPos())*(xf - posProxy->GetXPos()) + (yf - posProxy->GetYPos())*(yf - posProxy->GetYPos()));
 	
	if(dist>1){ // Proportional speed control
 		speed=STRAIGH_MAX_SPEED;
		return(false);
	}

 	if(dist<1 && dist>STRAIGHT_TOLERANCE){
 		speed= dist* STRAIGH_MAX_SPEED +STRAIGH_CRAWL_SPEED;
		return(false); 
	} 
 	
 
	if(dist<STRAIGHT_TOLERANCE){
		speed = 0; 
		turnrate = 0; 
		posProxy->SetSpeed(0,0); 
 		return(true);
	}else{
		return(false); 
	}
}
// ====== Turn 
#define TURN_MAX_SPEED 1. 
#define TURN_CRAWL_SPEED .01
#define TURN_TOLERANCE  .1
Turn::Turn(Position2dProxy* pp):Behaviours(pp){
	turn_to=0.;
}

Turn::~Turn(){
}

void Turn::Init(double a){
	angle = a; 
        turn_to=posProxy->GetYaw() + a;
	if(turn_to>PI) {turn_to=turn_to-PI_TWO;}; 
}

void Turn::Resume(void){
	Init(angle); 
}

bool Turn::Tick(void){
	double error=fabsf(turn_to-posProxy->GetYaw());
 
	if(error>1.) // Proportional turn speed control
 		turnrate=TURN_MAX_SPEED;
 	else
		turnrate=TURN_MAX_SPEED*error + TURN_CRAWL_SPEED;
 
	if(error<TURN_TOLERANCE){
		posProxy->SetSpeed(0,0); 
 		return(true);
 	}else{
 		return(false);
	}; 
}
// ===== NegTurn 
NegTurn::NegTurn(Position2dProxy* pp):Behaviours(pp){
	turn_to=0.;
}

NegTurn::~NegTurn(){
}

void NegTurn::Init(double a){
	angle = a; 
        turn_to=posProxy->GetYaw() + a;
	if(turn_to>PI) {turn_to=turn_to-PI_TWO;}; 
}

void NegTurn::Resume(void){
	Init(angle); 
}

bool NegTurn::Tick(void){
	double error=fabsf(turn_to-posProxy->GetYaw());
 
	if(error>1.) // Proportional turn speed control
 		turnrate=-TURN_MAX_SPEED;
 	else
		turnrate=-TURN_MAX_SPEED*error - TURN_CRAWL_SPEED;
 
	if(error<TURN_TOLERANCE){
		posProxy->SetSpeed(0,0); 
 		return(true);
 	}else{
 		return(false);
	}; 
}
// ===== Random
Random::Random(Position2dProxy* pp):Behaviours(pp){
	srand(time(NULL));
}

Random::~Random(){
}

void Random::Init(void){ 
	a = 0.1;
	turnrate = 1; 
} 

bool Random::Tick(void){ 
	sequence = rand() %100 - 50;
        turnrate = (1-a)*turnrate+a*sequence; 	
	speed = 0.5; 

	return(false); 
} 
// ===== MovePosition
MovePosition::MovePosition(Position2dProxy* pp):Behaviours(pp) {
	xf = 0;
	yf = 0; 
} 

MovePosition::~MovePosition(){
}

void MovePosition::Init(double x, double y) { 
	xf = x;
	yf = y;
} 

bool MovePosition::Tick(void){ 
	distance = sqrt((xf - posProxy->GetXPos())*(xf - posProxy->GetXPos()) + (yf - posProxy->GetYPos())*(yf - posProxy->GetYPos()));
	angle = atan2(yf - posProxy->GetYPos(),xf - posProxy->GetXPos());
	turnspeed = posProxy->GetYaw() - angle; 

	
	if (distance < 0.2){ 
		speed = 0 ;
		turnrate = 0; 
		posProxy->SetSpeed(speed,turnrate); 
		return(true); 
	}else{ 
		speed = 0.5; 
		if (turnspeed >= PI){
			turnrate = -(turnspeed-2*PI); 
			return(false);
		} else if (turnspeed < -PI){
			turnrate = -(turnspeed+2*PI);
			return(false);
		} else { 
			turnrate = -turnspeed;
			return(false);
		} 
	}
}
// ===== MoveNetwork 
MoveNetwork::MoveNetwork(Position2dProxy* pp):Behaviours(pp){ 
	X = 0;
	Y = 0; 
}

MoveNetwork::~MoveNetwork(){
} 

void MoveNetwork::Init(char* file){
	std::fstream myfile(file, std::ios_base::in);
	while(myfile >> a){
		Coords.push(a);
	}

	X = Coords.front();  
	Coords.pop();

	Y = Coords.front();
	Coords.pop();
} 

bool MoveNetwork::Tick(void){ 
	angle = atan2(Y-posProxy->GetYPos(),X-posProxy->GetXPos());
	distance = sqrt(((posProxy->GetXPos()- X)*(posProxy->GetXPos()- X)+(posProxy->GetYPos()- Y)*(posProxy->GetYPos()- Y)));
	double turnspeed = posProxy->GetYaw() - angle;


	if (Coords.empty() && distance <0.2) { posProxy->SetSpeed(0,0); return(true);}

	if (distance < 0.2){ 
		speed = 0 ;
		turnrate = 0; 
		posProxy->SetSpeed(speed,turnrate); 
		
		X = Coords.front();
		Coords.pop();

		Y = Coords.front();
		Coords.pop();
		return(false);  
	}else{ 
		speed = 0.5; 
		if (turnspeed >= PI){
			turnrate = -(turnspeed-2*PI); 
			return(false);
		} else if (turnspeed < -PI){
			turnrate = -(turnspeed+2*PI);
			return(false);
		} else { 
			turnrate = -turnspeed;
			return(false);
		} 
	}

} 
// ===== Wait 
Wait::Wait(Position2dProxy* pp):Behaviours(pp){}
Wait::~Wait(){}

// ===== Collison
Collision::Collision(Position2dProxy* pp, RangerProxy* rp){
	posProxy = pp; 
	ranger = rp; 
}

typedef struct ranger_map{
        double min_right;
        double right_angle;
        double min_left;
        double left_angle;
} ranger_map;


void compute_laser(RangerProxy* rp,ranger_map* r_map)
{
        uint32_t num =rp->GetRangeCount();

        r_map->min_right=100.;
        // MIN Right
        for (uint32_t i = 0; i <num/2; i++){
          if(((*rp)[i]) && (*rp)[i] < r_map->min_right){
            r_map->min_right=(*rp)[i];
            r_map->right_angle= rp->GetMinAngle() + i *rp->GetAngularRes();
           }
        }
        r_map->min_left=100.;
 	// MIN Left 
        for (uint32_t i = num ; i >num/2; i--){ // Scan other Way !
          if(((*rp)[i]) && (*rp)[i] < r_map->min_left){
            r_map->min_left=(*rp)[i];
            r_map->left_angle= rp->GetMinAngle() + (i+1) *rp->GetAngularRes();
           }
        }
}

double obst_avoid(RangerProxy* rp)
{

  // Determine the best way to turn
   double left_total,right_total;
   double weight;

        //first check if there is something directly in front, so we have to stop

        uint32_t num =rp->GetRangeCount();
        double weight_step = 1./(double)(rp->GetRangeCount()/2);

        //LEFT
        left_total=0.;
        weight=0.;
        for (uint32_t i =num/2 ; i <num; i++){
          if(((*rp)[i])){
                left_total+= (*rp)[i] * weight ;
           }
           weight+=weight_step;
        }

        // RIGHT
        right_total=0.;
        weight=1.;
        for (uint32_t i = 0; i <num/2; i++){
          if(((*rp)[i]) ){
            right_total+=(*rp)[i] *weight;
           }
           weight-=weight_step;
        }

	if(left_total<right_total) // Turn Right
		return(-1);
	else
		return(1);      // Turn Left
}
// ===== Laser
Laser::Laser(Position2dProxy* pp , RangerProxy* rp ):Collision(pp,rp)
{
    ranger->RequestGeom();
    ranger->RequestConfigure();
}

bool Laser::CheckMovement(double speed, double rotation){

	// Determine the point we want to move to
	if(speed==0)return(true); // We can always rotate

	const int num_ray=140;  // How much of Lasers Area I want To Use 
	bool result=false;
	const double safe_dist=.5;  // How close Do I want to Get To Wall
	for(int i = 0;i<num_ray;i++){
		if((*ranger)[i]<safe_dist){
			result=true;
			break;
		}
	}
        return(result);
}


double Laser::DirectionOfFreeSpace(void){
	ranger_map map_data;
	compute_laser(ranger,&map_data);
	double s = obst_avoid(ranger); 
	return(s);
}
// ===== Sonar 
Sonar::Sonar(Position2dProxy* pp, RangerProxy* rp):Collision(pp,rp){ 
	ranger->RequestGeom();
	ranger->RequestConfigure();
} 

bool Sonar::CheckMovement(double speed, double turnrate){
	return(true);
} 

double Sonar::DirectionOfFreeSpace(void){
	return(0.0); 
} 
// ===== Robot
Robot::Robot(PlayerClient* client, int index){
	posProxy = new Position2dProxy(client, index);
	ranger = new RangerProxy(client, 1); 
 
	standby = new Wait(posProxy); 
	current = ChangeBehaviour(); 
	collide = ChangeCollisionBehaviour(); 
	colCheck = new Laser(posProxy,ranger); 
}

double Robot::GetPosX(void){
	return (posProxy->GetXPos());
}

double Robot::GetPosY(void){
	return (posProxy->GetYPos()); 
} 

void Robot::Init(void){
	current = ChangeBehaviour();
	collide = ChangeCollisionBehaviour(); 
} 

void Robot::Tick(void){
	if(current->Tick()){
		//Change Behaviour
		current = this->ChangeBehaviour(); 
		current->Resume(); 
	}else{
		//Check Collision 
		double s = current->GetIntendedSpeed();
		double t = current->GetIntendedRotation(); 
		
		if(colCheck->CheckMovement(s,t)){
			collide = ChangeCollisionBehaviour();
			collide->Resume();
			collide->Tick();
			collide->DoMove(); 
			current->Resume(); 
		}else{ 
			current->DoMove(); 
		}
	} 
}

Behaviours* Robot::ChangeBehaviour(void){
	return standby; 
}

Behaviours* Robot::ChangeCollisionBehaviour(void){
	return standby; 
} 

//======= RobotPath 
RobotPath::RobotPath(PlayerClient* client, int index):Robot(client,index){
	go = new MoveNetwork(posProxy);
	((MoveNetwork*) go)->Init("path.txt");
	manu = new Turn(posProxy); 
	((Turn*) manu)->Init(0); 
	negmanu = new NegTurn(posProxy);
	((NegTurn*) negmanu)->Init(0);

	mode = S; 
}

Behaviours* RobotPath::ChangeBehaviour(void){
	return(go); 
}

Behaviours* RobotPath::ChangeCollisionBehaviour(void){
	double dir = colCheck->DirectionOfFreeSpace(); 
	if (dir == -1){mode = R;}else{mode = L;}; 
	if (mode==L){
		((Turn*) manu)->Init(PI/20); 
		return (manu); 
	}else{
		((Turn*) negmanu)->Init(-PI/20); 
		return (negmanu); 
	}
} 

//======= RobotMove 
RobotMove::RobotMove(PlayerClient* client, int index, double a, double b):Robot(client,index){
	go = new MovePosition(posProxy);
	((MovePosition*) go)->Init(a,b);
	manu = new Turn(posProxy); 
	((Turn*) manu)->Init(0); 
	negmanu = new NegTurn(posProxy);
	((NegTurn*) negmanu)->Init(0);

	mode=S; 
}

Behaviours* RobotMove::ChangeBehaviour(void){
	return(go); 
}

Behaviours* RobotMove::ChangeCollisionBehaviour(void){
	double dir = colCheck->DirectionOfFreeSpace(); 
	if (dir == -1){mode = R;}else{mode = L;}; 
	if (mode==L){
		((Turn*) manu)->Init(PI/20); 
		return (manu); 
	}else{
		((Turn*) negmanu)->Init(-PI/20); 
		return (negmanu); 
	}

} 
