#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream>


    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);

    uint32_t        gPort(60342);




void sonar(Position2dProxy& pp,RangerProxy& ranger){

      double valF = ranger[0];
      double valL = ranger[1];
      double valR = ranger[2];



      pp.SetSpeed(1,0);

      if(valF <= 1){

        if(valL < valR){
          pp.SetSpeed(0.01, 0.5);
        }

        if(valL > valR){
          pp.SetSpeed(0.01, -0.5);

        }
      }

     if(valF > 1 && valL <= 1){
        pp.SetSpeed(1,0.5);
     }

     if(valF > 1 && valR <= 1){
        pp.SetSpeed(1,-0.5);
     }

     if(valF <=0.9 && valL <= 0.9 && valR <= 0.9){
        pp.SetSpeed(0,5);
     }


}



int main(int argc, char **argv)
{


    try {
        PlayerClient robot(gHostname, gPort);
        Position2dProxy pp(&robot, 0);

        RangerProxy ranger(&robot, 0);
        robot.Read();


        ranger.RequestGeom();
        ranger.RequestConfigure();


        pp.SetMotorEnable (true);


    for(;;){

        robot.Read();
        sonar(pp,ranger);




     }


    }


    catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }

}
