// This program is inteneded to cause a bot to respond to obstavles in its path and avoid them. This is done through the use 
// of sonars. 

#include <libplayerc++/playerc++.h>
#include <iostream>

    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    uint32_t        gPort(63566); 

//Procedures 
void ReadSonar(Position2dProxy* pp, RangerProxy* ranger) {
} 

int main(int argc, char **argv)
{

  try {

    PlayerClient robot(gHostname, gPort); // Connect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)
    
    RangerProxy ranger(&robot, 0);
    robot.Read(); // Needed To Connect for sonar

    //Used To Populate Structure 
    ranger.RequestGeom();
    ranger.RequestConfigure();

    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors


    // Enter Loop
    for(;;){
      //Procedure Call 
      ReadSonar(pp,ranger); 

      // Blocks Until New Data Comes (10 Hz Default)
      robot.Read();
    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}
