#include <libplayerc++/playerc++.h>
#include <iostream>

    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    // NOTE !!! PLAYER_PORTNUM is the port number and has to be unique for each student,
   // and be set in the plaer server with -p
    uint32_t        gPort(63566); // Replace this with your port number !!



int main(int argc, char **argv)
{

  // we throw exceptions on creation if we fail
  try {

    PlayerClient robot(gHostname, gPort); // Conect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)

    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors

    //Variable Decleration
    double xpos0 = pp.GetXPos (),ypos0 = pp.GetYPos (),yaw0 = pp.GetYaw();
    double xpos = pp.GetXPos (), ypos = pp.GetYPos (), yaw = pp.GetYaw(); 
    double yaw1 = pp.GetYaw();
    
    int stage = 0;  
 
    // go into  a loop
    for(;;){
      //Update Postion
      xpos = pp.GetXPos ();
      ypos = pp.GetYPos ();
      yaw = pp.GetYaw (); 

      //Reset Default Speed 
      double speed = 0.2;
      double yawspeed = 0;  
      

      if (abs(xpos-xpos0)>=1 & stage ==0){ 
	while(yaw-yaw0<1.5){
	   yaw = pp.GetYaw();
	   pp.SetSpeed(0,0.1);
	   robot.Read();
        }
        yaw1 = pp.GetYaw();
        xpos0 = pp.GetXPos();
        robot.Read();
      } 

       if (abs(xpos-xpos0)>=1 & stage ==1){ 
	while(stage == 1){
	   yaw = pp.GetYaw();
	   pp.SetSpeed(0,0.05);
	     if (yaw>=-1.5 & yaw <= 2.5)  {stage = 2; yawspeed =0;} 
	   robot.Read();
        }
        yaw1 = pp.GetYaw();
        xpos0 = pp.GetXPos();
        robot.Read();
      } 

      if (abs(ypos-ypos0>=1 & stage == 0)){
	while((yaw-yaw1)<1.5){
	  yaw = pp.GetYaw();
	  pp.SetSpeed(0,0.1);
          robot.Read();
        } 
	stage = 1; 
	yaw1 = pp.GetYaw();
	ypos0 = pp.GetYPos();
        robot.Read();
      }

      if (abs(ypos-ypos0)>=1 & stage == 2){ 
	while(stage == 2){
	   yaw = pp.GetYaw();
	   pp.SetSpeed(0,0.05);
	     if (yaw >= 0)  {stage = 0; yawspeed=0;} 
	   robot.Read();
        }
        yaw1 = pp.GetYaw();
        ypos0 = pp.GetYPos();
        robot.Read();
      } 
           

      // this blocks until new data comes; 10Hz by default
      robot.Read();

      // write commands to robot
      pp.SetSpeed(speed,yawspeed); 
      
    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}











