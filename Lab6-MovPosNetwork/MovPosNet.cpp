#include <libplayerc++/playerc++.h>
#include <iostream>
#include <queue> 

    using namespace PlayerCc;

    std::string  gHostname(PlayerCc::PLAYER_HOSTNAME);
    uint32_t        gPort(63566);

//Constant Decleration 
double PI = 3.1415926535897;  

void queue(double x, double y,std::queue<double>* coorx, std::queue<double>* coory){

  coorx->push(x);
  coory->push(y);

}

void MoveToPosition (Position2dProxy* pp, double XF, double YF){ 
	double xPos = pp->GetXPos();
        double yPos = pp->GetYPos();
        double yaw = pp->GetYaw(); 

	double distance = sqrt((XF - xPos)*(XF - xPos) + (YF - yPos)*(YF - yPos));
	double angle = atan2(YF - yPos,XF - xPos); 
	double turnspeed = yaw - angle; 

	if (distance > 0.2){
		if (turnspeed >= PI){
			pp->SetSpeed(0.5, -(turnspeed-2*PI));
		} else if (turnspeed < -PI){
			pp->SetSpeed(0.5, -(turnspeed+2*PI));
		} else { 
			pp->SetSpeed(0.5, -turnspeed);
		} 
	} else { 
		 if (yaw < angle+0.15 && yaw > angle-0.15){  
                        pp->SetSpeed(0.0, 0);
                }
                else {
                        pp->SetSpeed(0.0, -PI/10);
                }
	}
} 

int main(int argc, char **argv)
{

  try {

    PlayerClient robot(gHostname, gPort); // Connect to server
    Position2dProxy pp(&robot, 0);   // Get a motor control device (index is 0)  
    
    std::queue<double> coorx; 
    std::queue<double> coory; 
	
    std::cout << robot << std::endl;

    pp.SetMotorEnable (true); // Turn on Motors

  // Initialize queue
    queue(-3,-6,&coorx,&coory);
    queue(0,-6,&coorx,&coory);
    queue(3.7,-4.4,&coorx,&coory);
    queue(4,-2,&coorx,&coory);
    queue(1.38,-1.73,&coorx,&coory);
    queue(-2,-1.14,&coorx,&coory);
    queue(-2.86,1.35,&coorx,&coory);
    queue(-1.13,2.89,&coorx,&coory);
    queue(2,3,&coorx,&coory);
    queue(2.6,5,&coorx,&coory);
    queue(1.46,6.95,&coorx,&coory);
    queue(-1,7,&coorx,&coory);
    queue(-2.59,6,&coorx,&coory);
    queue(-4.8,5.3,&coorx,&coory);
    queue(-6,3.4,&coorx,&coory);
    queue(-5,1,&coorx,&coory);
    queue(-3,-1,&coorx,&coory);
    queue(-3,-3,&coorx,&coory);
    queue(-4,-5,&coorx,&coory);

    // go into  a loop
    for(;;){
    
      
      // read next element and update X,Y 
      double X = coorx.front();
      double Y = coory.front(); 

      if (abs(pp.GetXPos() - X) <= 0.01 && abs(pp.GetYPos() - Y) <= 0.01){
      // puxh first into back and pop first element
 	  coorx.pop();
   	 coory.pop();

   	 queue(X,Y,&coorx,&coory);
      } 


      MoveToPosition(&pp,X,Y); 

      // this blocks until new data comes; 10Hz by default
      robot.Read();
    }
  }
  catch (PlayerCc::PlayerError & e) {
    std::cerr << e << std::endl;
    return -1;
  }
}
